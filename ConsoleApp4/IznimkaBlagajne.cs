﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4 {
    class IznimkaBlagajne : Exception {
        public IznimkaBlagajne(string poruka)
            : base(poruka) {
        }
    }
}
