﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4 {
    class Racun<T> : IRacun{
        public Racun(T iznosRacuna) {
            IznosRacuna = iznosRacuna;
            DatumIzdavanja = DateTime.Now;
        }


        public T IznosRacuna {
            get => iznosRacuna;
            set {
                if (Convert.ToDouble(value) < 10) {
                    Console.WriteLine("Iznos premal");
                }
                iznosRacuna = value;
            }
        }

        public DateTime DatumIzdavanja { get; set; }
        public decimal DohvatiIznos() {
            return Convert.ToDecimal(IznosRacuna);
        }

        public DateTime DohvatiDatumIzdavanja() {
            throw new NotImplementedException();
        }

        protected T iznosRacuna;
        
    }
}
